<?php
class Salary
{
    private $netSalary;
    public  $position;
    /** 
	 * Constructor for Salary Computations based from Position
	 *
	 * @param array $netSalary computed from NetSalary Function
	 * 
	 * @author Karen Cano <karen_cano@commmude.co.jp>
	 * 
	 * @return array output NetSalary
	 */
    public function __construct($position)
    {
        $this->position = $position;
    }

    /** 
	 * Function to determine the gross salary based from Position
	 *
	 * @param array $netSalary computed from NetSalary Function
	 * 
	 * @author Karen Cano <karen_cano@commmude.co.jp>
	 * 
	 * @return array output NetSalary
	 */
    public function SalaryPosition()
    {
        switch($this->position)
        {
            case "HR":
               $grossSalary = 40000;
               $this->netSalary = $this->NetSalary($grossSalary);
               return $this->netSalary;
            case "Janitor":
               $grossSalary = 30000;
               $this->netSalary = $this->NetSalary($grossSalary);
               return $this->netSalary;
            case "GM":
               $grossSalary = 20000;
               $this->netSalary = $this->NetSalary($grossSalary);
               return $this->netSalary;
            default:
               return $this->netSalary = 0;
        }
    }
    
    /** 
	 * Compute Net Salary
	 *
	 * @param array $netSalary computed from NetSalary Function
	 * 
	 * @author Karen Cano <karen_cano@commmude.co.jp>
	 * 
	 * @return array output NetSalary
	 */
    private function NetSalary($grossSalary)
    {
        $deductions = $grossSalary * 0.30;
        $netSalary = $grossSalary - $deductions;

        return array(
            "GrossSalary" => $grossSalary,
            "Deductions"   => $deductions,
            "NetSalary"   => $netSalary
        );
    }
}