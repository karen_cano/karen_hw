<?php

/* navigation menu */
function navigation($page)
{
	$param  = (filter_input(INPUT_GET, 'abcd',  FILTER_SANITIZE_STRING));
	$page   = ($param!='')
	        ? $param
	        : $page;
	$result = file_exists(INCLUDE_PATH . $page . '.' . DEFAULT_EXT)
	        ? $page
	        : 'error404';

	return INCLUDE_PATH . $result . '.' . DEFAULT_EXT;
}
