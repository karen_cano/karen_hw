<?php

class Employee extends Post
{
    public $position;

    public function EmployeeDetails()
    {
        switch(strtolower($this->postedFields["employee"]))
        {
            case "justin bieber":
                $this->position = "HR";
                return array(
                    "EmployeeName"      => "Justin Bieber",
                    "Position"          => $this->position,
                    "DateHired"         => "2016"
                );
            case "ken kasai":
                $this->position = "GM";
                return array(
                    "EmployeeName"      => "Ken Kasai",
                    "Position"          => $this->position,
                    "DateHired"         => "2017"
                );
            case "kuya juan":
                $this->position = "Janitor";
                return array(
                    "EmployeeName"      => "kuya juan",
                    "Position"          => $this->position,
                    "DateHired"         => "2015"
                );
            default:
                $this->position = "N/A";
                return array(
                    "EmployeeName"      => "N/A",
                    "Position"          => $this->position,
                    "Date Hired"        => "N/A"
                );
        }
    }
}//endof Employee Class