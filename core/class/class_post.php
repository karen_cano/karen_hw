<?php

/**
* Class to get properties from posted data from about.php
*
* @author    Karen Cano       <karen_cano@commmude.co.jp>
* @copyright 2017 Commude
* @version   0.1
* 
**/

class Post
{
    public $postedFields;
	public $renderToView;

	/** 
	 * Pass the posted/requested data 
	 *
	 * @param array $postedFields the data that is posted
	 * 
	 * @author Karen Cano <karen_cano@commmude.co.jp>
	 * 
	 * @return json output based from requested endpoint from abcd
	 */
    public function __construct()
	{
		$this->postedFields = $_REQUEST;
	}

	/** 
	 * Build the Object Properties based from the request.
	 *
	 * @param array $postedFields the data that is posted
	 * 
	 * @author Karen Cano <karen_cano@commmude.co.jp>
	 * 
	 * @return json output based from requested endpoint from abcd
	 */
	public function RouteResult()
	{
		switch($this->postedFields["abcd"])
		{
			case "about-submit":
				$companyClass = new Company();
				$companyDetails = $companyClass->CompanyDetails();
				$employeeClass = new Employee();
				$employeeDetails = $employeeClass->EmployeeDetails();
				$salaryClass = new Salary($employeeDetails["Position"]);
				$aboutSubmit =  array(
					"companyDetails" => $companyDetails,
					"employeeDetails"=> $employeeDetails,
					"salaryDetails"  => $salaryClass->SalaryPosition()
				);
				$jsonify = json_encode($aboutSubmit);
				return $jsonify;
			default:
			    break;
		}
	}

}//endof Post Class