<?php
require 'core/settings.php';

require CORE_PATH   . 'functions.php';

/* Head */
require COMMON_PATH . 'html-head.php'; 

/* Header */
require COMMON_PATH . 'header.php';

/* Content */
require navigation(DEFAULT_PAGE);

/* Footer */
require COMMON_PATH . 'footer.php';
