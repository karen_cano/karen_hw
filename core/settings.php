<?php
 /* URL and PATH CONSTANTS */
define('PROJECT_BASE_FOLDER',  substr($_SERVER['REQUEST_URI'], 0, 10) == '/karen_hw/' ? 'karen_hw/' : '');
define('PROJECT_FOLDER',       substr($_SERVER['REQUEST_URI'], 0, 10) == '/karen_hw/' ? ''              : ''); 
define('HTTP',            'http://');
define('LINK_PATTERN',    '?abcd=');

 //HOME PAGE
define('BASE_PATH',        $_SERVER['DOCUMENT_ROOT'] . '/' . PROJECT_BASE_FOLDER . PROJECT_FOLDER); 
define('SITE_URL',  HTTP . $_SERVER['SERVER_NAME']   . '/' . str_replace($_SERVER['DOCUMENT_ROOT'] . '/', '', BASE_PATH));

//CORE FILES
define('CORE_PATH',  BASE_PATH . 'core/');
define('CLASS_PATH', CORE_PATH . 'class/');

// COMMON PATH
define('INCLUDE_PATH', BASE_PATH    . 'include/');
define('COMMON_PATH',  INCLUDE_PATH . 'common/');

// NO NEED TO ADD .php
define('DEFAULT_PAGE', 'home'); 
define('DEFAULT_EXT',  'php');


/* CONTACT US (form) */
$contact_us = array(
	'fullname'	=> array(
					'LABEL'=>'Fullname'
					, 'value'=>''
					, 'type'=>'text'
					, 'required'=>1),
	'company'   => array(	
					'LABEL'=>'Company/Organization'
					, 'value'=>''
					, 'type'=>'text'
					, 'required'=>1),
	'email'     => array(
					'LABEL'=>'Email Address'
					, 'value'=>''
					, 'type'=>'email'
					, 'required'=>1),
	'contact'   => array(
					'LABEL'=>'Contact No.'
					, 'value'=>''
					, 'type'=>'text'
					, 'required'=>1),
	'inquiry'   => array(
					'LABEL'=>'Inquiry'
					, 'value'=>''
					, 'type'=>'textarea'
					,'required'=>1),
);

/* ABOUT US (form) */
$about_us = array(
	'employee'	=> array(
					'LABEL'=>'Employee Name'
					, 'value'=>'Justin Bieber'
					, 'type'=>'text'
					, 'required'=>0),
	'company'	=> array(
					'LABEL'=>'Company'
					, 'value'=>'COMMUDE'
					, 'type'=>'text'
					, 'required'=>0),

	'position'	=> array(
					'LABEL'=>'Position'
					, 'value'=>'HR'
					, 'type'=>'text'
					, 'required'=>0),
	// 'founder'   => array(
	// 				'LABEL'=>'Founder'
	// 				, 'value'=>'Toshiyuki Fukajima'
	// 				, 'type'=>'text'
	// 				, 'required'=>0),
	// 'industry'  => array(
	// 				'LABEL'=>'History'
	// 				, 'value'=>'IT'
	// 				, 'type'=>'email'
	// 				, 'required'=>0),
);





require_once('class/class_form.php');
require_once('class/class_post.php');
require_once('class/class_company.php');
require_once('class/class_employee.php');
require_once('class/class_salary.php');
global $form, $form2;

$form = new Form($contact_us);
$form2= new Form($about_us);
$route = new Post();


