<?php

class Company extends Post
{
    /** 
	 * Build the Company Properties
	 *
	 * @param array $postedFields the data that is posted
	 * 
	 * @author Karen Cano <karen_cano@commmude.co.jp>
	 * 
	 * @return array company output
	 */
    public function CompanyDetails()
    {
        switch(strtolower($this->postedFields["company"]))
        {
            case "commude":
                return array(
                    "CompanyName"        => "Commude PH",
                    "CEO"                => "Toshiyuki Fukajima",
                    "NatureOfBusiness"   => "IT solutions",
                    "YearEstablished"    => "2002"
                );
            case "HP":
                return array(
                    "CompanyName"        => "HP PH",
                    "CEO"                => "Meg Whitman",
                    "NatureOfBusiness"   => "BPO IT Industry",
                    "YearEstablished"    => "1980"
                );
            case "Accenture":
                return array(
                    "CompanyName"        => "Accenture PH",
                    "CEO"                => "Pierre Nanterme",
                    "NatureOfBusiness"   => "IT and BPO",
                    "YearEstablished"    => "2011"
                );
            default:
                return array(
                    "CompanyName"        => "N/A",
                    "CEO"                => "N/A",
                    "NatureOfBusiness"   => "N/A",
                    "YearEstablished"    => "N/A"
                );
        }
    }
}//endof Company Class