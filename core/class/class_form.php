<?php
/**
* Class to manage the form
*
* @author    Ken Kasai       <ken_kasai@commmude.co.jp>
* @copyright 2017 Commude
* @version   0.1
* 
**/

class Form
{
	protected $_fields;
	public    $_data = '';

	/** 
	 * Pass the data to property 
	 *
	 * @param array $datafield the data / field of the form
	 * 
	 * @author Ken Kasai <ken_kasai@commmude.co.jp>
	 * 
	 * @return string output / result of the form.
	 */
	public function __construct($datafield)
	{
		$this->_fields = $datafield;
		return $this->buildForm($this->_fields);
	}

	/** 
	 * For building the form
	 *
	 * @param array $datafield the data / field of the form
	 * 
	 * @author Ken Kasai <ken_kasai@commmude.co.jp>
	 * 
	 * @return string output / result of the form.
	 */
	public function buildForm($datafield)
	{
		foreach($datafield as $key => $value) {
			$required = ($value['required'] == 1) 
			          ? " <span class='required'>*</span>"
			          : "";
			$this->_data .= "<th>".$value['LABEL']. $required ."</th><td>";

			switch($value['type']) 
			{
				case 'textarea':
					$this->_data .= "<textarea id='".$key."' name='".$key."' value='".$value['value']."'></textarea>";
					break;
				case 'select':
					
					$this->_data .= "<select id='".$key."' name='".$key."'></select>";

					// option
					break;	
				default:
					$this->_data .= "<input type='".$value['type']."' id='".$key."' name='".$key."' value='".$value['value']."'>";
					break;
			} 
			$this->_data .= "</td><tr>";
		}

		return $this->_data;
	}

	/** 
	 * Display the form
	 *
	 * @author Ken Kasai <ken_kasai@commmude.co.jp>
	 * @return string output / result of the form.
	 */
	public function displayForm() 
	{
		return $this->_data;
	}
}