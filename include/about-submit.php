<?php

$aboutSubmitObj = json_decode($route->RouteResult());
echo "<table>";
echo "<tr><td>Employee Name    </td><td>:</td><td>" . $aboutSubmitObj->employeeDetails->EmployeeName . "</td></tr>";
echo "<tr><td>Company Name     </td><td>:</td><td>" . $aboutSubmitObj->companyDetails->CompanyName   . "</td></tr>";
echo "<tr><td>Founder/CEO      </td><td>:</td><td>" . $aboutSubmitObj->companyDetails->CEO           . "</td></tr>";
echo "<tr><td>Position         </td><td>:</td><td>" . $aboutSubmitObj->employeeDetails->Position     . "</td></tr>";
echo "<tr><td>Gross Salary     </td><td>:</td><td>" . "PHP " . number_format($aboutSubmitObj->salaryDetails->GrossSalary) . "</td></tr>";
echo "<tr><td>Deductions       </td><td>:</td><td>" . "PHP " . number_format($aboutSubmitObj->salaryDetails->Deductions)  . "</td></tr>";
echo "<tr><td>Net Salary       </td><td>:</td><td>" . "PHP " . number_format($aboutSubmitObj->salaryDetails->NetSalary)   . "</td></tr>";
echo "</table>";